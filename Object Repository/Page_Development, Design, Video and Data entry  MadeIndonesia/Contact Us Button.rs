<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Contact Us Button</name>
   <tag></tag>
   <elementGuidId>b7d0375c-51fd-4ee3-90c3-c209ea26a984</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[6]/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.NavItem__Wrapper__GkV2V.NavItem__Attention__qClm1 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a155a2b3-36cf-41f4-85de-1b88d4214cee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Contact us</value>
      <webElementGuid>74fb8683-b0b7-4515-8f58-563e2a431107</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[2]/div[@class=&quot;FloatingNavbar__Wrapper__2CxVQ&quot;]/div[@class=&quot;FloatingNavbar__NavContainer__w10PP&quot;]/a[6]/div[@class=&quot;NavItem__Wrapper__GkV2V NavItem__Attention__qClm1&quot;]/span[1]</value>
      <webElementGuid>8d57cd1a-7ec3-4706-b30f-3644839cdc04</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[2]/div/div/a[6]/div/span</value>
      <webElementGuid>eb6a09dc-5c7b-40ac-9132-ddd31b6a40a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collaboration'])[2]/following::span[1]</value>
      <webElementGuid>e155f65c-dc17-4859-9034-6e05e731c738</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About us'])[2]/following::span[2]</value>
      <webElementGuid>3866170c-9c3a-46c8-8418-3c2b54e3a1ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your browser does not support the video tag.'])[1]/preceding::span[3]</value>
      <webElementGuid>63b4d8c7-4971-4ded-9b3b-984c09420c4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A collaboration with Indonesia under Dutch project supervision.'])[1]/preceding::span[3]</value>
      <webElementGuid>2f09cc59-029a-45b2-966a-7f8ede91525a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Contact us']/parent::*</value>
      <webElementGuid>f7a33fca-4e5d-40aa-b066-b67d8a6d4401</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[6]/div/span</value>
      <webElementGuid>4ea95151-9713-4580-bed3-bc461a95ac9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Contact us' or . = 'Contact us')]</value>
      <webElementGuid>7d27e79f-1bf5-4724-b186-f9be1f52f47b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
